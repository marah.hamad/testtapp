//
//  ProvidersVCPresenter.swift
//  ProgressSoftApp
//
//  Created by Marah Hamad on 3/15/19.
//  Copyright © 2019 ProgressSoft. All rights reserved.
//

import Foundation

protocol ProvidersVCPresenter : NSObjectProtocol{
    
    func githubJobsRequestSuccessful(_ jobList:[Job])
    func githubJobsRequestFailed(_ error:String)
    
    func searchGovJobsRequestSuccessful(_ jobList:[Job])
    func searchGovJobsRequestFailed(_ error:String)
    
    func getAllJobsRequestSuccessful(_ jobList:[Job],_ query: String?)
    func getAllJobsRequestFailed(_ error:String)
    
}

final class MainViewPresenter {
    weak fileprivate var viewPresenter : ProvidersVCPresenter?
    
    init(_ view:ProvidersVCPresenter) {
        viewPresenter = view
    }
    func getAllJobsRequest(position: String?, location: String?, query: String?) {
        
        Dialogs.showLoading()
        APIClient.request(api: API.gitHubJobs(position: position, location: location)) { [weak self] (jobList:[Job]?, response:DataResponse<Data>?, cache:APIClient.DataStatus) in
            Dialogs.dismiss()
            guard let `self` = self else {
                return
            }
            if jobList != nil{
                
                self.viewPresenter?.getAllJobsRequestSuccessful(jobList!,query)
            }else {
                self.viewPresenter?.getAllJobsRequestFailed("Internel server error")
            }
        }
        
    }
    
    
    func githubJobsRequest(position: String?, location: String?) {
        
        Dialogs.showLoading()
        APIClient.request(api: API.gitHubJobs(position: position, location: location)) { [weak self] (jobList:[Job]?, response:DataResponse<Data>?, cache:APIClient.DataStatus) in
            Dialogs.dismiss()
            guard let `self` = self else {
                return
            }
            if jobList != nil{
                
                self.viewPresenter?.githubJobsRequestSuccessful(jobList!)
            }else {
                self.viewPresenter?.githubJobsRequestFailed("Internel server error")
            }
        }
        
    }
    
    func searchGovJobsRequest(query: String?){
        
        Dialogs.showLoading()
        APIClient.request(api: API.searchGovJobs(query: query)) { [weak self] (jobList:[Job]?, response:DataResponse<Data>?, cache:APIClient.DataStatus) in
            Dialogs.dismiss()
            guard let `self` = self else {
                return
            }
            if jobList != nil{
                
                self.viewPresenter?.searchGovJobsRequestSuccessful(jobList!)
            }else {
                self.viewPresenter?.searchGovJobsRequestFailed("Internel server error")
            }
        }
        
    }
    
    
}

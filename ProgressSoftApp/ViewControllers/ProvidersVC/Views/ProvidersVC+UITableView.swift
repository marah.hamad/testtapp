//
//  ProvidersVC+UITableView.swift
//  ProgressSoftApp
//
//  Created by Marah Hamad on 3/15/19.
//  Copyright © 2019 ProgressSoft. All rights reserved.
//

import Foundation

extension ProvidersVC : ProvidersVCPresenter {
    
    //MARK:- All Jobs
    
    func getAllJobsRequestSuccessful(_ jobList: [Job], _ query: String?) {
        
        self.allJobs = jobList
        self.presenter.searchGovJobsRequest(query: query)
        
    }
    
    func getAllJobsRequestFailed(_ error: String) {
        Dialogs.showError(error)
        
    }
    
    //MARK:- Github Jobs
    
    func githubJobsRequestSuccessful(_ jobList:[Job]){
        self.githubJobs = jobList
    }
    func githubJobsRequestFailed(_ error:String){
        Dialogs.showError(error)
    }
    
    //MARK:- Search Gov Jobs
    
    func searchGovJobsRequestSuccessful(_ jobList:[Job]){
        self.searchGovJobs = jobList
        self.allJobs = allJobs + jobList
        self.providersTableView.reloadData()
    }
    func searchGovJobsRequestFailed(_ error:String){
        Dialogs.showError(error)
        
    }
    
    
}


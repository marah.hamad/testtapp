//
//  ProvidersVC+Presenter.swift
//  ProgressSoftApp
//
//  Created by Marah Hamad on 3/15/19.
//  Copyright © 2019 ProgressSoft. All rights reserved.
//

import Foundation
extension ProvidersVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch flag {
        case 0:
            return allJobs.count
        case 1:
            return githubJobs.count
        case 2:
            return searchGovJobs.count
            
        default:
            return allJobs.count
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTVC", for: indexPath) as! CustomTVC
        switch flag {
        case 0:
            cell.fill(allJobs[indexPath.row])
        case 1:
            cell.fill(githubJobs[indexPath.row])
        case 2:
            cell.fill(searchGovJobs[indexPath.row])
        default:
            cell.fill(allJobs[indexPath.row])
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch flag {
        case 0:
            if let url = URL(string: allJobs[indexPath.row].url ?? "") {
                UIApplication.shared.open(url, options: [:])
            }
        case 1:
            if let url = URL(string: githubJobs[indexPath.row].url ?? "") {
                UIApplication.shared.open(url, options: [:])
            }
        case 2:
            if let url = URL(string: searchGovJobs[indexPath.row].url ?? "") {
                UIApplication.shared.open(url, options: [:])
            }
        default:
            if let url = URL(string: allJobs[indexPath.row].url ?? "") {
                UIApplication.shared.open(url, options: [:])
            }
        }
        
        
    }
    
    
}


//
//  ProvidersVC.swift
//  ProgressSoftApp
//
//  Created by Marah Hamad on 3/14/19.
//  Copyright © 2019 ProgressSoft. All rights reserved.
//

import UIKit

class ProvidersVC: UIViewController {

   
    @IBOutlet weak var locationView: UIView!
    var flag : Int = 0
    
    @IBOutlet weak var jobTitleTF: UITextField!
    @IBOutlet weak var locationTF: UITextField!
    @IBOutlet weak var providersTableView: UITableView!{
        didSet{
    
            self.providersTableView.register(UINib(nibName: "CustomTVC", bundle: nil), forCellReuseIdentifier: "CustomTVC")
            self.githubJobs = [Job]()
            self.searchGovJobs = [Job]()
            self.allJobs = [Job]()
            self.providersTableView.reloadData()
            flag = 0
            self.presenter.getAllJobsRequest(position: "", location: "", query: "")
            
        }
    }
    
    lazy var presenter = MainViewPresenter(self)
    var githubJobs = [Job]()
    var searchGovJobs = [Job]()
    var allJobs = [Job]()
    
    // Google Places UI
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    
    @IBOutlet weak var locationSearchBar: UISearchBar!
    
    var menuView: BTNavigationDropdownMenu!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let items = ["All Providers", "Github", "Search Gov"]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 23.0/255.0, green:81/255.0, blue:143/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: BTTitle.index(0), items: items)
        
        
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 182.0/255.0, green:186.0/255.0, blue:197.0/255.0, alpha: 1.0)
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> Void in
            print("Did select item at index: \(indexPath)")
            self.flag = indexPath
        }
        
        self.navigationItem.titleView = menuView
        
        
    }
    
    @IBAction func searchAction(_ sender: Any) {
        
        self.githubJobs = [Job]()
        self.searchGovJobs = [Job]()
        self.allJobs = [Job]()
        
        switch flag {
        case 0:
            
            self.presenter.getAllJobsRequest(position: jobTitleTF.text, location: locationTF.text, query: "\(jobTitleTF.text!)+\(locationTF.text!)")
        case 1:
            
            self.presenter.githubJobsRequest(position: jobTitleTF.text, location: locationTF.text)
        case 2:
            
            self.presenter.searchGovJobsRequest(query: "\(jobTitleTF.text!)+\(locationTF.text!)")
        default:
            
            self.presenter.getAllJobsRequest(position: jobTitleTF.text, location: locationTF.text, query: "\(jobTitleTF.text!)+\(locationTF.text!)")
        }
        
    }
}

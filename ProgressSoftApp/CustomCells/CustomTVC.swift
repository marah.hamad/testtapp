//
//  CustomTVC.swift
//  ProgressSoftApp
//
//  Created by Marah Hamad on 3/14/19.
//  Copyright © 2019 ProgressSoft. All rights reserved.
//

import UIKit

class CustomTVC: UITableViewCell {

    
    @IBOutlet weak var logoIMG: UIImageView!
    @IBOutlet weak var jobTitleLbl: UILabel!
    @IBOutlet weak var companyNameLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var postDateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func fill(_ job:Job) {
        
        if (job.title ?? "").isEmpty {
            
            logoIMG.sd_setImage(with: URL(string: job.companyLogo ?? ""), placeholderImage: UIImage(named: "SmallLogo"))
            jobTitleLbl.text = job.positionTitle
            companyNameLbl.text = job.organizationName
            locationLbl.text = job.locations?[0]
            postDateLbl.text = job.startDate
            
        }
            
        else {
            logoIMG.sd_setImage(with: URL(string: job.companyLogo ?? ""), placeholderImage: UIImage(named: "SmallLogo"))
            jobTitleLbl.text = job.title
            companyNameLbl.text = job.company
            locationLbl.text = job.location
            postDateLbl.text = job.createdAt
        }
        
        
        
    }
}

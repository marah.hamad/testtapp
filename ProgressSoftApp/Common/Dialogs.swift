//
//  Dialogs.swift
//  iOS-Task
//
//  Created by Marah Hamad on 3/14/19.
//  Copyright © 2019 ProgressSoft. All rights reserved.
//

import Foundation

class Dialogs {
    
    // MARK:- KVNProgress
    static func loadingDialogConfiguration(){
        let configuration = KVNProgressConfiguration.init()
        
        configuration.statusColor = UIColor.themeColor
        configuration.statusFont = UIFont.systemFont(ofSize: 13)
        configuration.circleStrokeForegroundColor = UIColor.themeColor
        configuration.circleStrokeBackgroundColor = UIColor.themeColor.withAlphaComponent(0.3)
        configuration.backgroundFillColor = UIColor.white.withAlphaComponent(0.9)
        configuration.backgroundTintColor = UIColor.white.withAlphaComponent(1)
        configuration.successColor = UIColor.themeColor
        configuration.errorColor = UIColor.themeColor
        configuration.stopColor = UIColor.themeColor
        configuration.minimumDisplayTime = 0.3
        configuration.minimumSuccessDisplayTime = 3.0
        configuration.minimumErrorDisplayTime = 15.0
        configuration.isFullScreen = false
        configuration.doesShowStop = false
        configuration.stopRelativeHeight = 0.4
        
        configuration.tapBlock = { progressView in
            if progressView?.style != KVNProgressStyle.progress {
                DispatchQueue.main.sync {
                    KVNProgress.dismiss()
                }
            }
        }
        KVNProgress.setConfiguration(configuration)
    }
    
    static func dismiss(){
        DispatchQueue.main.async {
            KVNProgress.dismiss()
        }
    }
    
    static func showLoading(_ message:String? = nil){
        DispatchQueue.main.async {
            KVNProgress.dismiss()
            if let text = message {
                KVNProgress.show(withStatus: text)
            }else {
                KVNProgress.show()
            }
        }
    }
    
    static func showSuccess(_ message:String? = nil){
        DispatchQueue.main.async {
            KVNProgress.dismiss()
            if let text = message {
                KVNProgress.showSuccess(withStatus: text)
            }else {
                KVNProgress.showSuccess()
            }
        }
    }
    
    static func showError(_ message:String? = nil){
        DispatchQueue.main.async {
            KVNProgress.dismiss()
            if let text = message {
                KVNProgress.showError(withStatus: text)
            }else {
                KVNProgress.showError()
            }
        }
    }
}

extension UIColor {
      static let themeColor = colorHelper(hex: "17518F")

}

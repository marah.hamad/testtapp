//
//  APIClient.swift
//  iOS-Task
//
//  Created by Marah Hamad on 3/14/19.
//  Copyright © 2019 ProgressSoft. All rights reserved.
//

import Foundation

struct APIClient {
    
    //MARK:- cache
    static func limitCache(MemorySize memory:Int,DiskSize disk:Int) {
        URLCache.shared = URLCache(memoryCapacity: memory * 1024 * 1024, diskCapacity: disk * 1024 * 1024, diskPath: nil)
    }
    
    //MARK:- request
    @discardableResult static func request<T: Decodable>(api: API, completion: @escaping (T?,_ response:DataResponse<Data>?,_ dataStatus :DataStatus) -> ()) -> Bool {
        return request(api: api, completion: completion, failed: { (failed:APIFailed?) in
            print("Request failed")
        })
    }
    
    @discardableResult static func request<T: Decodable, F: Decodable>(api: API, completion: @escaping (T?,_ response:DataResponse<Data>?,_ dataStatus :DataStatus) -> (), failed: @escaping (F?) -> ()) -> Bool {
        //check if url is valied
        guard let url = api.url else {
            print("request failed: Can't find URL")
            return Alamofire.NetworkReachabilityManager.shared?.isReachable ?? false
        }
        
        //setup URLRequest
        let request = api.skipInvalidCertificate ?  sessionManager.request(url, method: api.method, parameters: api.parameter, encoding: api.parameterEncoding, headers: api.header) : Alamofire.request(url, method: api.method, parameters: api.parameter, encoding: api.parameterEncoding, headers: api.header)
        if let authenticate = api.authenticate {
            request.authenticate(usingCredential: authenticate)
        }
        
        if let timeout = api.timeout {
            request.session.configuration.timeoutIntervalForRequest = timeout
        }
        
        //Cache
        if api.method == HTTPMethod.get ,
            api.ignorCache == false ,
            let req = request.request,
            let cachedResponse = URLCache.shared.cachedResponse(for: req){
            let data = cachedResponse.data
            do {
                let obj = try JSONDecoder().decode(T.self, from: data)
                completion(obj,DataResponse.init(request: req, response: cachedResponse.response as? HTTPURLResponse, data: data, result: Result.success(data)) ,DataStatus.cachedData)
            } catch _ {
            }
        }
        
        //send request
        if Alamofire.NetworkReachabilityManager.shared?.isReachable ?? false == true {
            request.validate().responseData { (response) in
                Response(dataResponse: response, Api: api, completion: completion, failed: failed)
            }
            
        }
        return Alamofire.NetworkReachabilityManager.shared?.isReachable ?? false
    }
    
  
    
    //MARK:- Handle invalid certificate
    open class MyServerTrustPolicyManager: ServerTrustPolicyManager {
        open override func serverTrustPolicy(forHost host: String) -> ServerTrustPolicy? {
            return ServerTrustPolicy.disableEvaluation
        }
    }
    
    static let sessionManager = SessionManager(delegate:SessionDelegate(), serverTrustPolicyManager:MyServerTrustPolicyManager(policies: [:]))
    
}

extension NetworkReachabilityManager {
    static let shared = NetworkReachabilityManager.init()
}

//
//  API.swift
//  iOS-Task
//
//  Created by Marah Hamad on 3/14/19.
//  Copyright © 2019 ProgressSoft. All rights reserved.
//

import Foundation

enum API {
    
     static let githubDomain = "https://jobs.github.com/"
     static let searchGovDomain = "https://jobs.search.gov/"
    
    case gitHubJobs(position:String?,location:String?)
    case searchGovJobs(query:String?)

    var route : String {
        var url = ""
        switch self {
        case .gitHubJobs:
            url = API.githubDomain
        case .searchGovJobs:
            url = API.searchGovDomain.appending("jobs/")
    
        }
        return url
    }

    var url : URL? {
        var url = route
        switch self {
        case .gitHubJobs:
            url = url.appending("positions.json")
        case .searchGovJobs:
            url = url.appending("search.json")
      
        }
        return URL.init(string: url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? "")
    }

}

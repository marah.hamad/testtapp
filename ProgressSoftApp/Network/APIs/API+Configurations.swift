//
//  API+Configurations.swift
//  iOS-Task
//
//  Created by Marah Hamad on 3/14/19.
//  Copyright © 2019 ProgressSoft. All rights reserved.
//

import Foundation

extension API {
    
    var parameterEncoding : ParameterEncoding {
        switch self {
        default:
            return URLEncoding.default //JSONEncoding.default
        }
    }
    
    var authenticate : URLCredential? {
        switch self {
        default:
            /* Sample
             return URLCredential.init(user: "admin@domin.com", password: "pass", persistence: URLCredential.Persistence.forSession)
             */
            return nil
        }
    }
    
    var timeout : TimeInterval? {
        switch self {
        default:
            return 120
        }
    }
    
    var ignorCache : Bool {
        switch self {
        default:
            return true//false
        }
    }
    
    var skipInvalidCertificate : Bool {
        switch self {
        default:
            return true
        }
    }
    
}

//
//  API+Fields.swift
//  iOS-Task
//
//  Created by Marah Hamad on 3/14/19.
//  Copyright © 2019 ProgressSoft. All rights reserved.
//

import Foundation

extension API {
    
    var method : HTTPMethod {
        switch self {
        case .gitHubJobs,.searchGovJobs:
            return HTTPMethod.get
      
        }
    }
    
    var header : [String:String]? {
        switch self {
        case .gitHubJobs,.searchGovJobs:
            return ["Content-type": "application/x-www-form-urlencoded"]
     
        }
    }
    
    var parameter : [String:Any]? {
        switch self {
        case .gitHubJobs(let position, let location):
            return ["position":position ?? "" ,"location":location ?? ""]
        case .searchGovJobs(let query):
            return ["query":query ?? ""]
        }
    }
    }
